# -*- coding: utf-8 -*-

"""
Copyright (C) 2022 Corsair M360, Inc - All Rights Reserved.
Unauthorized copying of this file, via any medium is strictly prohibited.
Proprietary and confidential.
"""

from scripts import forms
from scripts import helpers
from flask import Flask, redirect, url_for, render_template, request, session
import json
import os

app = Flask(__name__)

################################################################################
#                         M360 Middleware Setup                                #
################################################################################
"""
 Provide the 4 options below when the microservice that consumes this middleware is intended to run in a kubernetes
 deployment
 @platform {String} value equals 'kubernetes'
 @namespace {String} value equals the kubernetes namespace where your deployment will run
 @service {String} value equals the name of the kubernetes service that is attached to the your deployment
 @exposedPort {String} value equals the exposed port of your kubernetes service
 @example
      'platform': 'kubernetes',
      'platformOptions": {
          'namespace': 'mike',
          'service': 'service-express',
          'exposedPort': os.environ.get('APP_PORT') or 8000
      }
"""
"""
Provide the 4 options below when the microservice that consumes this middleware is intended to run in a docker service
@platform {String} value equals 'docker'
@network {String} value equals the docker network attached to your docker service
@service {String} value equals the name of the docker service
@example
    'platform': 'docker',
    'platformOptions": {
        'network': 'mike',
        'service': 'service-express',
    }
"""
from m360.frameworks.flask.middleware import FlaskMiddleware
app.wsgi_app = FlaskMiddleware(app.wsgi_app, {
    "contract": os.path.join(os.path.dirname(__file__), "./contract.json"),
    "ip":  os.environ.get('APP_IP') or "127.0.0.1",
    "type": "flask",
    "platform": "manual"
})
from m360 import sdk
M360 = sdk.instance()
################################################################################

app.secret_key = os.urandom(12)  # Generic key for dev purposes only

# ======== Routing =========================================================== #
# -------- Login ------------------------------------------------------------- #
@app.route('/', methods=['GET', 'POST'])
def login():
    if not session.get('logged_in'):
        form = forms.LoginForm(request.form)
        if request.method == 'POST':
            username = request.form['username'].lower()
            password = request.form['password']
            if form.validate():
                if helpers.credentials_valid(username, password):
                    session['logged_in'] = True
                    session['username'] = username
                    return json.dumps({'status': 'Login successful'})
                return json.dumps({'status': 'Invalid user/pass'})
            return json.dumps({'status': 'Both fields required'})
        return render_template('login.html', form=form)
    user = helpers.get_user()
    return render_template('home.html', user=user)


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return redirect(url_for('login'))


# -------- Signup ---------------------------------------------------------- #
@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if not session.get('logged_in'):
        form = forms.LoginForm(request.form)
        if request.method == 'POST':
            username = request.form['username'].lower()
            password = helpers.hash_password(request.form['password'])
            email = request.form['email']
            if form.validate():
                if not helpers.username_taken(username):
                    helpers.add_user(username, password, email)
                    session['logged_in'] = True
                    session['username'] = username
                    return json.dumps({'status': 'Signup successful'})
                return json.dumps({'status': 'Username taken'})
            return json.dumps({'status': 'User/Pass required'})
        return render_template('login.html', form=form)
    return redirect(url_for('login'))


# -------- Settings ---------------------------------------------------------- #
@app.route('/settings', methods=['GET', 'POST'])
def settings():
    if session.get('logged_in'):
        if request.method == 'POST':
            password = request.form['password']
            if password != "":
                password = helpers.hash_password(password)
            email = request.form['email']
            helpers.change_user(password=password, email=email)
            return json.dumps({'status': 'Saved'})
        user = helpers.get_user()
        return render_template('settings.html', user=user)
    return redirect(url_for('login'))

# -------- M360 ---------------------------------------------------------- #
@app.route('/m360/settings', methods=['GET', 'POST'])
def m360_settings():
    return json.dumps(M360.registry.get())

@app.route('/m360/databases', methods=['GET', 'POST'])
def get_databases():
    tenants = M360.tenant.list(request)
    single_dbs = M360.database.get("single")
    mt_dbs = M360.database.get("multitenant", None, tenants[0].get("code")) if tenants and len(tenants) > 0 \
        else None
    return json.dumps({"single": single_dbs, "multitenant": mt_dbs})

@app.route('/m360/resources', methods=['GET', 'POST'])
def get_resources():
    return json.dumps(M360.resource.get())

@app.route('/m360/users', methods=['GET', 'POST'])
def get_users():
    return json.dumps(M360.user.find({}))

@app.route('/m360/user/me', methods=['GET', 'POST'])
def get_user():
    return json.dumps(M360.user.get(request))

@app.route('/m360/user', methods=['GET', 'POST'])
def get_user_by_id():
    return json.dumps(M360.user.find({"id": request.args.get("id")}))

@app.route('/m360/tenants', methods=['GET', 'POST'])
def get_tenants():
    return json.dumps(M360.tenant.list(request))

@app.route('/m360/tenants/search', methods=['GET', 'POST'])
def find_tenants():
    return json.dumps(M360.tenant.find(request.args.getlist("codes")))

@app.route('/m360/awareness', methods=['GET', 'POST'])
def get_awareness():
    return json.dumps(M360.awareness.get())

@app.route('/m360/awareness/discovery', methods=['GET', 'POST'])
def get_next_host():
    return json.dumps(M360.awareness.get_next_host("flaskapp", 1))

@app.route('/m360/proxy', methods=['GET', 'POST'])
def proxy():
    proxy_res = M360.awareness.proxy({
        "user": "5f23b5fff05042751d6fea9d",
        "service": "flaskapp",
        "version": 1,
        "route": "/heartbeat",
        "method": "get"
    })
    return json.dumps(proxy_res)

@app.route('/m360/contract', methods=['GET', 'POST'])
def get_contract():
    return json.dumps(M360.config.get("contract"))

# ======== Main ============================================================== #
if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, host="0.0.0.0", port=8000, ssl_context='adhoc')
