# Sample Flask App for M360 middleware testing #

## Python env setup ##

1. Install **pyenv** (using homebrew or other) to be able to easily switch between Python versions and create Python virtual envs
2. Install a Python version using **pyenv**:
    ```
    pyenv install 3.7.7 
    ```
3. Create the virtual env for this specific project and activate it:
    ```
    pyenv virtualenv 3.7.7 flask-app-3.7
    pyenv activate flask-app-3.7
    ```
4. Install the required packages:
    ```
    pip install -r requirements.txt
    ```

## Flask App Testing ##

To install the M360 Python middleware:
```
pip install m360-middleware
```

To launch the app:
```
APP_SSL_CERT=/path/to/cert.pem APP_SSL_KEY=/path/to/key.pem LOG_LEVEL=debug GATEWAY_MAINTENANCE_IP=https://192.168.1.118 GATEWAY_MAINTENANCE_PORT=5000 FLASK_RUN_PORT=8000 python3 app.py
```

## Notes ##

The M360 middleware is configured in `flaskex/app.py` (lines 17-56).
