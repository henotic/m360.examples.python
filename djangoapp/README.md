# Sample Django App for M360 middleware testing #

## Python env setup ##

1. Install **pyenv** (using homebrew or other) to be able to easily switch between Python versions and create Python virtual envs
2. Install a Python version using **pyenv**:
    ```
    pyenv install 3.7.7 
    ```
3. Create the virtual env for this specific project and activate it:
    ```
    pyenv virtualenv 3.7.7 django-app-3.7
    pyenv activate django-app-3.7
    ```
4. Install the required packages:
    ```
    pip3 install --upgrade pip
    pip3 install -r requirements.txt
    ```

## Django App Testing ##

To install the M360 Python middleware, build the wheel file in that repo and then do:
```
pip install m360-middleware
```

To launch the app:
```
APP_SSL_CERT=cert.pem APP_SSL_KEY=key.pem LOG_LEVEL=debug GATEWAY_MAINTENANCE_IP=https://192.168.1.118 GATEWAY_MAINTENANCE_PORT=5000 python3 manage.py runserver_plus 0.0.0.0:8000 --cert-file cert.pem --key-file key.pem
```

When running for the first time, you might need to setup the SQLite3 DB using:
```
python3 manage.py migrate
```

## Notes ##

The M360 middleware is already configured in `djangoapp/settings.py`.
The middleware path is included in the `MIDDLEWARE` list and the `M360` config is also setup in there.

## SSL ##

This app has been modified to use HTTPS using [this guide](https://timonweb.com/django/https-django-development-server-ssl-certificate/).

