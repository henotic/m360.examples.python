"""djangoapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('m360/settings', views.get_settings),
    path('m360/databases', views.get_databases),
    path('m360/resources', views.get_resources),
    path('m360/users', views.get_users),
    path('m360/user/me', views.get_user),
    path('m360/user', views.get_user_by_id),
    path('m360/tenants', views.get_tenants),
    path('m360/tenants/search', views.find_tenants),
    path('m360/awareness', views.get_awareness),
    path('m360/awareness/discovery', views.get_next_host),
    path('m360/proxy', views.proxy),
    path('m360/contract', views.get_contract),
]
