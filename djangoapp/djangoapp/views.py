from django.conf import settings
from django.http import JsonResponse

from m360 import sdk
M360 = sdk.instance()

def get_settings(request):
    return JsonResponse(M360.registry.get(), status=200, safe=False)

def get_databases(request):
    tenants = M360.tenant.list(request)
    single_dbs = M360.database.get("single")
    mt_dbs = M360.database.get("multitenant", None, tenants[0].get("code")) if tenants and len(tenants) > 0 \
        else None
    return JsonResponse({"single": single_dbs, "multitenant": mt_dbs}, status=200, safe=False)

def get_resources(request):
    return JsonResponse(M360.resource.get(), status=200, safe=False)

def get_user(request):
    return JsonResponse(M360.user.get(request), status=200, safe=False)

def get_tenants(request):
    return JsonResponse(M360.tenant.list(request), status=200, safe=False)

def find_tenants(request):
    return JsonResponse(M360.tenant.find(request.GET.getlist("codes")), status=200, safe=False)

def get_users(request):
    return JsonResponse(M360.user.find({}), status=200, safe=False)

def get_user_by_id(request):
    return JsonResponse(M360.user.find({"id": request.GET.get("id")}), status=200, safe=False)

def get_awareness(request):
    return JsonResponse(M360.awareness.get(), status=200, safe=False)

def get_next_host(request):
    return JsonResponse(M360.awareness.get_next_host("djangoapp", 1), status=200, safe=False)

def proxy(request):
    proxy_res = M360.awareness.proxy({
        "user": "5f23b5fff05042751d6fea9d",
        "service": "djangoapp",
        "version": 1,
        "route": "/heartbeat",
        "method": "get"
    })
    return JsonResponse(proxy_res, status=200, safe=False)

def get_contract(request):
    return JsonResponse(M360.config.get("contract"), status=200, safe=False)
