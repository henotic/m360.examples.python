"""
Copyright (C) 2022 Corsair M360, Inc - All Rights Reserved.
Unauthorized copying of this file, via any medium is strictly prohibited.
Proprietary and confidential.
"""

import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
DIR_NAME = os.path.dirname(__file__)
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-3pgt6$3mi4do+*27p&3m)(p6a)$ege4rqyhnf8e-y+gno4n#a0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # -------------------- M360 Middleware Setup --------------------
    'm360.frameworks.django.middleware.DjangoMiddleware',
]

# -------------------- M360 Middleware Config --------------------
"""
     Provide the 4 options below when the microservice that consumes this middleware is intended to run in a kubernetes
     deployment
     @platform {String} value equals 'kubernetes'
     @namespace {String} value equals the kubernetes namespace where your deployment will run
     @service {String} value equals the name of the kubernetes service that is attached to the your deployment
     @exposedPort {String} value equals the exposed port of your kubernetes service
     @example
          'platform': 'kubernetes',
          'platformOptions": {
              'namespace': 'mike',
              'service': 'service-express',
              'exposedPort': os.environ.get('APP_PORT') or 8000
          }
"""
"""
     Provide the 4 options below when the microservice that consumes this middleware is intended to run in a docker
     service
     @platform {String} value equals 'docker'
     @network {String} value equals the docker network attached to your docker service
     @service {String} value equals the name of the docker service
     @example
          'platform': 'docker',
          'platformOptions": {
              'network': 'mike',
              'service': 'service-express',
          }
"""
M360 = {
    "contract": os.path.join(DIR_NAME, "./contract.json"),
    "ip":  os.environ.get('APP_IP') or "127.0.0.1",
    "type": "django",
    "platform": "manual"
}
# ----------------------------------------------------------------

ROOT_URLCONF = 'djangoapp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'djangoapp.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
