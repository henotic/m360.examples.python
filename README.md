# m360.examples.python

---

The M360 Python Examples contains examples of how to use the M360 Middleware with your Python Microservices & APIs.

The M360 Middleware is developed using native Python and acts as a dependency that gets consumed by a Python Web Server.
The examples include running the Middleware with [Django](https://www.djangoproject.com/) and [Flask](https://flask.palletsprojects.com/en/2.0.x/).


## Installation ##

---

```
pip install m360-middleware
```

## Examples ##

---

* [Django Sample App](https://bitbucket.org/henotic/m360.examples.python/src/master/djangoapp/README.md)
* [Flask Sample App](https://bitbucket.org/henotic/m360.examples.python/src/master/flaskex/README.md)


**Reference:** [M360 Middleware Official Documentation](https://corsairm360.atlassian.net/servicedesk/customer/portal/4/topic/419cca91-5815-447b-abde-8455ae8a1717)
